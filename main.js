
        'use strict';

        function HamburgerException (message) {
            this.message = "Hamburger exeption: " + message;
        };   
                                      
        // const SIZE_SMALL = {name: 'SIZE_SMALL', price: 50, calories: 20};
        // const SIZE_LARGE = {name: 'SIZE_LARGE', price: 100, calories: 40};
        // const STUFFING_CHEESE = {name: 'STUFFING_CHEESE', price: 10, calories: 20};
        // const STUFFING_SALAD = {name: 'STUFFING_SALAD', price: 20, calories: 5};
        // const STUFFING_POTATO = {name: 'STUFFING_POTATO', price: 15, calories: 10};
        // const TOPPING_MAYO = {name: 'TOPPING_MAYO', price: 20, calories: 5};
        // const TOPPING_SPICE = {name: 'TOPPING_SPICE', price: 15, calories: 0};
        
        class Hamburger {
            constructor (size, stuffing, toppings) {
             if (size !== Hamburger.SIZE_SMALL && size !== Hamburger.SIZE_LARGE && size !== undefined) {
                    throw new HamburgerException("invalid size " + size.name);
                };
            
             if (size === undefined) {
                    throw new HamburgerException("no size given");
                }; 

            this.size = size;

            if(stuffing !== Hamburger.STUFFING_CHEESE && stuffing !== Hamburger.STUFFING_SALAD && 
                            stuffing !== Hamburger.STUFFING_POTATO && stuffing !== undefined){
                                throw new HamburgerException("invalid stuffing " + stuffing.name);
                            };
             if (stuffing === undefined) {
                    throw new HamburgerException("no stuffing given");  
                };

             

            this.stuffing = stuffing;
            
        
            this.toppings = [];
            }

           static SIZE_SMALL = {name: 'SIZE_SMALL', price: 50, calories: 20};
           
           static SIZE_LARGE = {name: 'SIZE_LARGE', price: 100, calories: 40};
           
           static STUFFING_CHEESE = {name: 'STUFFING_CHEESE', price: 10, calories: 20};

           static STUFFING_SALAD = {name: 'STUFFING_SALAD', price: 20, calories: 5};
           
           static STUFFING_POTATO = {name: 'STUFFING_POTATO', price: 15, calories: 10};
           
           static TOPPING_MAYO = {name: 'TOPPING_MAYO', price: 20, calories: 5};

           static TOPPING_SPICE = {name: 'TOPPING_SPICE', price: 15, calories: 0};
           

            addTopping = function (topping) {

                let nameDobleTopping = this.toppings.find(function(element){return element === topping});
 
                if(topping !== Hamburger.TOPPING_MAYO && topping !== Hamburger.TOPPING_SPICE){
                    throw new HamburgerException("invalid topping " + topping.name);
                }
 
                if (this.toppings.length > 2){
                    throw new HamburgerException("You cant add more topping")
                }
                         
                if (nameDobleTopping !== undefined){
                    throw new HamburgerException("double topping " + topping.name);
                };
        
                this.toppings.push(topping);
            };
                                        
            removeTopping = function (topping){
                    let removeIndex= this.toppings.indexOf(topping);
                    if (this.toppings.length === 0){
                        throw new HamburgerException("You try remove nothing");
                    };
                                    
                    if (removeIndex === -1){
                        throw new HamburgerException("This kind of topping is absent");
                    }; 
    
                    if (this.topping !== Hamburger.TOPPING_MAYO && topping !== Hamburger.TOPPING_SPICE){
                        throw new HamburgerException("You try remove unknown topping");
                    };
    
                    this.toppings.splice(removeIndex, 1);
                }

            getToppings = function (){
                    return this.toppings;
                }

            getSize = function () {
                    return this.size;
                }
                    
            getStuffing = function () {
                        return this.stuffing;
                    } 

                    calculatePrice = function (){
                       let price = this.size.price + this.stuffing.price;
      
                        if (this.toppings !== undefined) {
                          for (let topping of this.toppings) {
                              price += topping.price;
                            }
                        }
              
                      return price;
                    }
                    
                    calculateCalories = function (){
                    let calories = this.size.calories + this.stuffing.calories;
                   
                        if (this.toppings !== undefined) {
                            for (let topping of this.toppings) {
                                calories += topping.calories;
                            }
                        }
                        return calories;
                    }
                }; 

                   
try{
     // маленький гамбургер с начинкой из сыра
    let hamburger =  new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
console.log(hamburger);

//добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);

// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит? 
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер? 
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1

//не передали обязательные параметры
let h2 = new Hamburger(); 

// передаем некорректные значения, добавку вместо размера
let h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE); 
// => HamburgerException: invalid size 'TOPPING_SAUCE'

// добавляем много добавок
let h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
h4.addTopping(Hamburger.TOPPING_MAYO);
h4.addTopping(Hamburger.TOPPING_MAYO); 
//HamburgerException: duplicate topping 'TOPPING_MAYO'
} catch(err){
    console.error(err.message)
};